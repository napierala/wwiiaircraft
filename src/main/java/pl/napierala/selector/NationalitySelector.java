package pl.napierala.selector;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.data.Property;
import com.vaadin.data.util.converter.Converter;
import com.vaadin.ui.*;
import pl.napierala.ds.NationalityJPAContainer;
import pl.napierala.model.Nationality;

/**
 * A custom field to implement a comboBox of a persisted resourece, the Nationality in this case.
 */
public class NationalitySelector extends CustomField<Nationality> {

    /**
     * The comboBox of the nationalities.
     */
    private ComboBox nationalityComboBox = new ComboBox();

    /**
     * The Nationalities JPAContainer
     */
    private JPAContainer<Nationality> container;

    public NationalitySelector() {
        container = new NationalityJPAContainer();

        setCaption("Nationality");
        nationalityComboBox.setContainerDataSource(container);

        //Choose the field that will be shown to the user.
        nationalityComboBox.setItemCaptionPropertyId("name");

        //Create a listener to implement the selecting of a nationality.
        nationalityComboBox.addListener(new Property.ValueChangeListener() {
            @Override
            public void valueChange(
                    com.vaadin.data.Property.ValueChangeEvent event) {

                /*
                 * Modify the actual value of the custom field.
                 */
                if (nationalityComboBox.getValue() == null) {
                    setValue(null, false);

                } else {
                    //Get the nationality using the value of the ComboBox(the id of the nationality).
                    Nationality entity = container.getItem(nationalityComboBox.getValue()).getEntity();

                    setValue(entity, false);
                }
            }
        });
    }

    @Override
    protected Component initContent() {
        CssLayout cssLayout = new CssLayout();
        cssLayout.addComponent(nationalityComboBox);
        return cssLayout;
    }

    @Override
    public void setPropertyDataSource(Property newDataSource) {
        super.setPropertyDataSource(newDataSource);
        setNationality((Nationality) newDataSource.getValue());
    }

    @Override
    public void setValue(Nationality newValue) throws ReadOnlyException,
            Converter.ConversionException {
        super.setValue(newValue);
        setNationality(newValue);
    }

    private void setNationality(Nationality nationality) {
        nationalityComboBox
                .setValue(nationality != null ? nationality.getId() : null);
    }

    @Override
    public Class<? extends Nationality> getType() {
        return Nationality.class;
    }


}
