package pl.napierala.model;


import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "nationality")
public class Nationality implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @NotNull
    @Size(min=2, max=50)
    /**
     * The nationalities name, e.g. USA
     */
    private String name;

    @URL
    /**
     * The url for the nationality icon, e.g. http://nationalFlags.com/USA.png
     */
    private String nationalityUrl = "";

    public Nationality(String name, String nationalityUrl) {
        this.name = name;
        this.nationalityUrl = nationalityUrl;
    }

    public Nationality(){

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNationalityUrl() {
        return nationalityUrl;
    }

    public void setNationalityUrl(String nationalityUrl) {
        this.nationalityUrl = nationalityUrl;
    }
}
