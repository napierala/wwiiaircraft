package pl.napierala.model;

import org.hibernate.validator.constraints.URL;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;


@Entity
@Table(name = "aircraft")
public class Aircraft implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id = 0;

    @NotNull
    @Size(min = 2, max = 100)
    /**
     * The name of the aircraft, e.g. B-17.
     */
    private String name = "";

    @NotNull
    @Size(min = 2, max = 100)
    /**
     * The type of the aircraft, e.g. Fighter.
     */
    private String type = "";

    @Version
    private Integer version;

    @ManyToOne
    /**
     * The aircraft's nationality.
     */
    private Nationality nationality;

    public Aircraft(Integer id, String name, String type, Integer version, Nationality nationality) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.version = version;
        this.nationality = nationality;
    }

    public Aircraft() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public Nationality getNationality() {
        return nationality;
    }

    public void setNationality(Nationality nationality) {
        this.nationality = nationality;
    }

}
