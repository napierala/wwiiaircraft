package pl.napierala.demo;


import pl.napierala.ds.NationalityJPAContainer;
import pl.napierala.model.Nationality;

/**
 * A demo generator to get some values for the nationalities.
 */
public class DemoNationalityGenerator {

    final static Nationality[] demoNationalities = {
            new Nationality("Germany", "http://upload.wikimedia.org/wikipedia/commons/thumb/9/99/Flag_of_German_Reich_%281935%E2%80%931945%29.svg/23px-Flag_of_German_Reich_%281935%E2%80%931945%29.svg.png"),
            new Nationality("USA", "http://upload.wikimedia.org/wikipedia/en/thumb/a/a4/Flag_of_the_United_States.svg/23px-Flag_of_the_United_States.svg.png"),
            new Nationality("Soviet Union", "http://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Flag_of_the_Soviet_Union.svg/23px-Flag_of_the_Soviet_Union.svg.png")
    };


    /**
     * Create the nationalities(persist them by commiting the JPAContainer).
     */
    public static void create() {

        //Ge the container
        NationalityJPAContainer nationalityJPAContainer = new NationalityJPAContainer();

        //Go through all of the demo nationalities.
        for(Nationality nationality : demoNationalities) {

            //Add the nationality to the JPAContainer
            nationalityJPAContainer.addEntity(nationality);

        }

        //Commit to save the changes.
        nationalityJPAContainer.commit();
    }
}
