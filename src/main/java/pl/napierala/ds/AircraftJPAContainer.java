package pl.napierala.ds;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.provider.CachingMutableLocalEntityProvider;
import pl.napierala.model.Aircraft;

import javax.persistence.EntityManager;
import java.util.Collection;

/**
 * A specific JPAContainer for the Aircraft.
 */
public class AircraftJPAContainer extends JPAContainer<Aircraft> {


    /**
     * The persistance UNIT(from persistance.xml).
     */
    private static final String PERSISTENCE_UNIT = "ww2AircraftUnit";

    public AircraftJPAContainer() {
        super(Aircraft.class);
        EntityManager em = JPAContainerFactory.createEntityManagerForPersistenceUnit(PERSISTENCE_UNIT);
        setEntityProvider(new CachingMutableLocalEntityProvider<Aircraft>(Aircraft.class, em));
    }

    @Override
    public Collection<Filter> getContainerFilters() {
        return null;
    }
}
