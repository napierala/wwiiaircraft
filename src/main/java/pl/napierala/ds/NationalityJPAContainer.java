package pl.napierala.ds;

import com.vaadin.addon.jpacontainer.JPAContainer;
import com.vaadin.addon.jpacontainer.JPAContainerFactory;
import com.vaadin.addon.jpacontainer.provider.CachingMutableLocalEntityProvider;
import pl.napierala.model.Nationality;

import javax.persistence.EntityManager;
import java.util.Collection;

/**
 * A specific JPAContainer for the Nationality.
 */
public class NationalityJPAContainer extends JPAContainer<Nationality> {

    /**
     * The persistance UNIT(from persistance.xml).
     */
    private static final String PERSISTENCE_UNIT = "ww2AircraftUnit";

    public NationalityJPAContainer() {
        super(Nationality.class);
        EntityManager em = JPAContainerFactory.createEntityManagerForPersistenceUnit(PERSISTENCE_UNIT);
        setEntityProvider(new CachingMutableLocalEntityProvider<Nationality>(Nationality.class, em));
    }

    @Override
    public Collection<Filter> getContainerFilters() {
        return null;
    }
}
