package pl.napierala.ui;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.ui.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vaadin.dialogs.ConfirmDialog;
import pl.napierala.ds.AircraftJPAContainer;
import pl.napierala.model.Aircraft;
import pl.napierala.selector.NationalitySelector;

/**
 * The window to work on the aircraft(edit and create).
 */
public class AircraftWindow extends Window implements Button.ClickListener {

    static final Logger logger = LoggerFactory.getLogger(AircraftWindow.class);

    private FormLayout layout;
    /**
     * A binder to bind fields in the form to specific field in the Aircraft model.
     */
    private BeanFieldGroup<Aircraft> binder;

    private HorizontalLayout buttons;
    private Button saveButton;
    private Button cancelButton;
    private Button deleteButton;

    /**
     * The Aircraft JPAContainer
     */
    private AircraftJPAContainer datasource;

    public AircraftWindow(AircraftJPAContainer datasource) {
        this.datasource = datasource;
        init();
        setModal(true);
    }

    /**
     * Initialize this window.
     */
    private void init() {
        layout = new FormLayout();
        layout.setSizeFull();
        layout.setSpacing(true);

        saveButton = new Button("Save");
        saveButton.addClickListener(this);
        saveButton.setClickShortcut(KeyCode.ENTER);

        cancelButton = new Button("Cancel");
        cancelButton.addClickListener(this);
        cancelButton.setClickShortcut(KeyCode.ESCAPE);

        deleteButton = new Button("Delete");
        deleteButton.addClickListener(this);
        deleteButton.setVisible(false);

        buttons = new HorizontalLayout();
        buttons.addComponent(saveButton);
        buttons.addComponent(cancelButton);
        buttons.addComponent(deleteButton);

        setContent(layout);

        setHeight("370");
        setWidth("400");
    }

    /**
     * When this method is called, a edit window will be created for editing the aircraft.
     *
     * @param id
     *          The id of the aircraft to edit.
     */
    public void edit(Integer id) {
        try {

            setCaption("Edit Aircraft");

            //Get the aircraft that will be edited from the datasource
            Aircraft m = datasource.getItem(id).getEntity();

            //Bind all of the fields that will be edited to the form's elements.
            bindingFields(m);

            deleteButton.setVisible(true);
            UI.getCurrent().addWindow(this);
        } catch (Exception ex) {
            logger.error("Error while editing a aircraft. ", ex);
            Notification.show("Error while editing: " + ex.getMessage(), Notification.Type.ERROR_MESSAGE);
        }
    }

    /**
     * When this method is called, a create window will be created for creating a aircraft.
     *
     */
    public void create() {
        setCaption("New Aircraft");

        //Bind the field to a brand new aircraft that is still empty.
        bindingFields(new Aircraft());

        UI.getCurrent().addWindow(this);
    }

    private void bindingFields(Aircraft m) {

        //Make the binder for the aircraft object
        binder = new BeanFieldGroup<Aircraft>(Aircraft.class);

        //Set the dataSource
        binder.setItemDataSource(m);

        //The name of the aircraft
        TextField nameTextField = new TextField("Name");

        nameTextField.setInputPrompt("e.g. B-17");
        binder.bind(nameTextField, "name");
        nameTextField.setWidth("140");

        layout.addComponent(nameTextField);

        //The type of the aircraft

        TextField typeTextField = new TextField("Type");

        typeTextField.setInputPrompt("e.g. Bomber");
        binder.bind(typeTextField, "type");
        typeTextField.setWidth("200");

        layout.addComponent(typeTextField);


        //The nationality of the aircraft.
        NationalitySelector nationalitySelectorField = new NationalitySelector();

        binder.bind(nationalitySelectorField, "nationality");
        layout.addComponent(nationalitySelectorField);

        layout.addComponent(buttons);
    }


    @Override
    public void buttonClick(Button.ClickEvent event) {
        if (event.getButton() == saveButton) { //If the saveButton was clicked.

            try {
                //Commit the binder to check if the validation passes.
                binder.commit();

            } catch (FieldGroup.CommitException e) {
                Notification.show("Error while commiting.");
                return;
            }

            try {
                datasource.addEntity(binder.getItemDataSource().getBean());
                Notification.show("Aircraft persisted!", Notification.Type.HUMANIZED_MESSAGE);

            } catch (Exception e) {
                logger.debug("Exception while persisting a aircraft!", e);
                Notification.show("Exception while persisting a aircraft!\n" + e
                        .getMessage(), Notification.Type.ERROR_MESSAGE);
                return;
            }
        } else if (event.getButton() == deleteButton) { //If the deleteButton was clicked.

            //Create a dialog to confirm this big step.
            ConfirmDialog.show(this.getUI(), "Are you sure you want to delete this aircraft?",
                    new ConfirmDialog.Listener() {
                        public void onClose(ConfirmDialog dialog) {
                            if (dialog.isConfirmed()) { //If we have a confirmation.
                                try {

                                    //Remove this item
                                    datasource.removeItem(binder.getItemDataSource().getBean().getId());

                                } catch (Exception e) {
                                    logger.debug("Exception while removing a aircraft!", e);
                                    Notification.show("Exception while removing a aircraft! \n" + e
                                            .getMessage(), Notification.Type.ERROR_MESSAGE);
                                }
                                close();
                            }
                        }
                    });
            return;
        } else if (event.getButton() == cancelButton) { //If the cancel button was clicked.

            binder.discard(); //Discard everything from this form.
        }
        close();
    }
}
