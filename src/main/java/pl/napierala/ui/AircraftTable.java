package pl.napierala.ui;

import com.vaadin.data.Property;
import com.vaadin.server.ExternalResource;
import com.vaadin.ui.Image;
import com.vaadin.ui.Table;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import pl.napierala.ds.AircraftJPAContainer;
import pl.napierala.model.Nationality;

/**
 * The Aircraft Table.
 */
public class AircraftTable extends Table {

    static final Logger logger = LoggerFactory.getLogger(AircraftTable.class);

    public AircraftTable(AircraftJPAContainer aircraftContainer) {
        setContainerDataSource(aircraftContainer);

        setHeight("450px");
        setWidth("650px");
        configColumns();
    }

    /**
     * Config the columns of the table
     */
    private void configColumns() {
        setVisibleColumns(new Object[]{"id", "name", "type"});
        setColumnHeaders(new String[]{"#", "Name", "Type"});

        //Add a generated column to have a image in the table using a url from the aircraft's nationality.
        addGeneratedColumn("Nationality", new NationalityColumnGenerator());
    }

    private class NationalityColumnGenerator implements ColumnGenerator {
        @Override
        public Object generateCell(Table source, Object itemId, Object columnId) {

            try {
                String nationalityUrl = null;

                //Get the nationality of the row that the generator is working on.
                Object nationality = source.getItem(itemId).getItemProperty("nationality").getValue();

                if (nationality != null) {
                    nationalityUrl = ((Nationality) nationality).getNationalityUrl();
                }

                if (nationalityUrl != null && !nationalityUrl.isEmpty()) {

                    //Get the picture from the url provided.
                    Image nationalityImage = new Image("Nationality", new ExternalResource(nationalityUrl));

                    return nationalityImage;
                }
                return null;
            } catch (Exception ex) {
                logger.error("Exception while generating the nationality image.");
                return null;
            }
        }
    }
}
